package cz.tomrechtig.ejb.definition;

import cz.tomrechtig.api.entities.definition.IRequestOrder;
import cz.tomrechtig.domain.entities.definition.IOrder;

import javax.ejb.Remote;
import java.util.List;

@Remote
public interface IOrderSessionRemote {

    List<IOrder> getAllOrders();

    List<IOrder> getOrdersByUserId(Long userId);

    boolean addBooks(Long orderId, List<Long> bookIds);

    boolean removeBooks(Long orderId, List<Long> bookIds);

    boolean createOrder(IRequestOrder requestOrder);

    boolean deleteOrder(Long orderId);

}
