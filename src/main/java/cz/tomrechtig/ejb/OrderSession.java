package cz.tomrechtig.ejb;

import cz.tomrechtig.api.entities.definition.IRequestOrder;
import cz.tomrechtig.data.entities.definition.IOrderDAO;
import cz.tomrechtig.ejb.definition.IOrderSessionRemote;
import cz.tomrechtig.data.entities.BookDAO;
import cz.tomrechtig.data.entities.OrderDAO;
import cz.tomrechtig.data.services.definition.IBookService;
import cz.tomrechtig.data.services.definition.IOrderService;
import cz.tomrechtig.domain.entities.definition.IOrder;
import cz.tomrechtig.domain.mappers.definition.IOrderMapper;

import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.List;
import java.util.stream.Collectors;

@Stateless
public class OrderSession implements IOrderSessionRemote {

    @Inject
    IOrderService orderService;

    @Inject
    IOrderMapper orderMapper;

    @Inject
    IBookService bookService;

    @Override
    public List<IOrder> getAllOrders() {
        List<OrderDAO> orderDAOList = orderService.getAllOrders();
        return orderDAOList.stream().map(orderDAO -> orderMapper.toOrder(orderDAO)).collect(Collectors.toList());
    }

    @Override
    public List<IOrder> getOrdersByUserId(Long userId) {
        List<OrderDAO> orderDAOList = orderService.getAllOrdersByUserId(userId);
        return orderDAOList.stream().map(orderDAO -> orderMapper.toOrder(orderDAO)).collect(Collectors.toList());
    }

    @Override
    public boolean addBooks(Long orderId, List<Long> bookIds) {
        try {
            List<BookDAO> bookDAOList = bookIds.stream().map(bookId -> bookService.getBookById(bookId)).collect(Collectors.toList());
            orderService.addBooks(orderId, bookDAOList);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public boolean removeBooks(Long orderId, List<Long> bookIds) {
        try {
            orderService.removeBooks(orderId, bookIds);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public boolean createOrder(IRequestOrder requestOrder) {
        try {
            IOrderDAO orderDAO = orderMapper.toDAO(requestOrder);
            orderService.createOrder(orderDAO);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public boolean deleteOrder(Long orderId) {
        try {
            orderService.deleteOrder(orderId);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
}
