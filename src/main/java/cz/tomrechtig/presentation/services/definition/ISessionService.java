package cz.tomrechtig.presentation.services.definition;

import cz.tomrechtig.presentation.entities.definition.ICurrentUser;

public interface ISessionService {

    void login(String email, String password);

    void logout();

    boolean isUserLoggedIn();

    ICurrentUser getCurrentUser();

}
