package cz.tomrechtig.presentation.services;

import cz.tomrechtig.domain.services.definition.IAuthService;
import cz.tomrechtig.presentation.entities.definition.ICurrentUser;
import cz.tomrechtig.presentation.exceptions.InvalidPasswordException;
import cz.tomrechtig.presentation.exceptions.UserNotFoundException;
import cz.tomrechtig.presentation.services.definition.ISessionService;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

@ApplicationScoped
public class SessionService implements ISessionService {

    @Inject
    IAuthService authService;

    private ICurrentUser currentUser;

    @Override
    public void login(String email, String password) {
        try {
            this.currentUser = authService.login(email, password);
        } catch (UserNotFoundException userNotFoundException) {
            System.out.println("USER NOT FOUND");
        } catch (InvalidPasswordException invalidPasswordException) {
            System.out.println("INVALID PASSWORD");
        }
    }

    @Override
    public void logout() {
        this.currentUser = null;
    }

    @Override
    public boolean isUserLoggedIn() {
        return currentUser != null;
    }

    @Override
    public ICurrentUser getCurrentUser() {
        return this.currentUser;
    }
}
