package cz.tomrechtig.presentation.entities.definition;

public interface IRegisterFormUser {

    String getEmail();
    void setEmail(String email);

    String getUsername();
    void setUsername(String username);

    String getPassword();
    void setPassword(String password);

}
