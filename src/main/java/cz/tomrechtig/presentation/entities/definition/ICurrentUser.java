package cz.tomrechtig.presentation.entities.definition;

import cz.tomrechtig.domain.entities.definition.IBook;
import cz.tomrechtig.domain.entities.definition.IUser;

import java.util.List;

public interface ICurrentUser extends IUser {

    List<IBook> getCart();

    int getCartSize();

    void addToCart(IBook book);

    void removeFromCart(IBook book);

}
