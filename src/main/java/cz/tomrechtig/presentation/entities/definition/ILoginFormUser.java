package cz.tomrechtig.presentation.entities.definition;

public interface ILoginFormUser {

    String getEmail();
    void setEmail(String email);

    String getPassword();
    void setPassword(String password);

}
