package cz.tomrechtig.presentation.entities;

import cz.tomrechtig.presentation.entities.definition.ILoginFormUser;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Named
@RequestScoped
public class LoginFormUser implements ILoginFormUser {

    @Email(message = "Email is not valid")
    @NotBlank(message = "Email can't be blank")
    private String email;

    @Size(min = 8, max = 32, message = "Password must contain 8 - 16 characters")
    private String password;

    public LoginFormUser() {}

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
