package cz.tomrechtig.presentation.entities;

import cz.tomrechtig.domain.entities.definition.IBook;
import cz.tomrechtig.domain.entities.definition.IUser;
import cz.tomrechtig.presentation.entities.definition.ICurrentUser;

import java.util.ArrayList;
import java.util.List;

public class CurrentUser implements ICurrentUser {

    private Long id;

    private String username;

    private String email;

    private String password;

    private final List<IBook> cart;

    public CurrentUser(Long id, String username, String email, String password, List<IBook> cart) {
        this.id = id;
        this.username = username;
        this.email = email;
        this.password = password;
        this.cart = cart;
    }

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String getUsername() {
        return username;
    }

    @Override
    public void setUsername(String username) {
        this.username = username;
    }

    @Override
    public String getEmail() {
        return email;
    }

    @Override
    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public List<IBook> getCart() {
        return cart;
    }

    @Override
    public int getCartSize() {
        return cart.size();
    }

    @Override
    public void addToCart(IBook book) {
        cart.add(book);
    }

    @Override
    public void removeFromCart(IBook book) {
        cart.remove(book);
    }
}
