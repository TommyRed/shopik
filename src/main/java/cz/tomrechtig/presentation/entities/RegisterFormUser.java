package cz.tomrechtig.presentation.entities;

import cz.tomrechtig.presentation.entities.definition.IRegisterFormUser;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Named
@RequestScoped
public class RegisterFormUser implements IRegisterFormUser {

    @Email(message = "Email is not valid")
    @NotBlank(message = "Email can't be blank")
    private String email;

    @NotBlank(message = "Username can't be blank")
    private String username;

    @Size(min = 8, max = 32, message = "Password must contain 8 - 16 characters")
    private String password;

    public RegisterFormUser() {}

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
