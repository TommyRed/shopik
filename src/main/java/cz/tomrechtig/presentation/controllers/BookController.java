package cz.tomrechtig.presentation.controllers;

import cz.tomrechtig.data.entities.BookDAO;
import cz.tomrechtig.data.services.definition.IBookService;
import cz.tomrechtig.domain.entities.definition.IBook;
import cz.tomrechtig.domain.mappers.definition.IBookMapper;
import cz.tomrechtig.presentation.controllers.definition.IBookController;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.List;

@Named
@ApplicationScoped
public class BookController implements IBookController {

    @Inject
    IBookService bookService;

    @Inject
    IBookMapper bookMapper;

    @Override
    public List<IBook> getBooks() {
        List<BookDAO> bookDAOs = bookService.getAllBooks();
        return bookMapper.toBook(bookDAOs);
    }
}
