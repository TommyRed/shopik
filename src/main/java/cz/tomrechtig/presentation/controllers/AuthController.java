package cz.tomrechtig.presentation.controllers;

import cz.tomrechtig.data.entities.UserDAO;
import cz.tomrechtig.data.entities.definition.IUserDAO;
import cz.tomrechtig.domain.services.definition.IAuthService;
import cz.tomrechtig.jms.definition.IJMSProducer;
import cz.tomrechtig.presentation.controllers.definition.IAuthController;
import cz.tomrechtig.presentation.entities.definition.ILoginFormUser;
import cz.tomrechtig.presentation.entities.definition.IRegisterFormUser;
import cz.tomrechtig.presentation.services.definition.ISessionService;
import cz.tomrechtig.presentation.utilities.definition.IRequestUtility;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.inject.Named;

@Named
@ApplicationScoped
public class AuthController implements IAuthController {

    @Inject
    IAuthService authService;

    @Inject
    ISessionService sessionService;

    @Inject
    IRequestUtility requestUtility;

    @Inject
    IJMSProducer jmsProducer;

    public void register(IRegisterFormUser user) {
        try {
            IUserDAO registeredUser = new UserDAO(user.getUsername(), user.getEmail(), user.getPassword());
            authService.register(registeredUser);
            jmsProducer.send("[FE] User [" + user.getEmail() + "] has registered.");
            requestUtility.redirect("");
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    @Override
    public void login(ILoginFormUser loginFormUser) {
        try {
            this.sessionService.login(loginFormUser.getEmail(), loginFormUser.getPassword());
            jmsProducer.send("[FE] User [" + loginFormUser.getEmail() + "] has logged in.");
            requestUtility.redirect("");
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
}
