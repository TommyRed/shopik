package cz.tomrechtig.presentation.controllers.definition;

import cz.tomrechtig.presentation.entities.definition.ILoginFormUser;
import cz.tomrechtig.presentation.entities.definition.IRegisterFormUser;

public interface IAuthController {

    void register(IRegisterFormUser user);

    void login(ILoginFormUser loginFormUser);

}
