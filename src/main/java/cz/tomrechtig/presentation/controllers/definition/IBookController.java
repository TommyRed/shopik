package cz.tomrechtig.presentation.controllers.definition;

import cz.tomrechtig.domain.entities.definition.IBook;

import java.util.List;

public interface IBookController {
    List<IBook> getBooks();
}
