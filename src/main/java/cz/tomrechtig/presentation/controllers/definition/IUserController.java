package cz.tomrechtig.presentation.controllers.definition;

import cz.tomrechtig.domain.entities.definition.IBook;
import cz.tomrechtig.domain.entities.definition.IOrder;
import cz.tomrechtig.presentation.entities.definition.ICurrentUser;

import java.util.List;

public interface IUserController {
    ICurrentUser getCurrentUser();

    boolean isLoggedIn();

    void logout();

    void addToCart(IBook book);

    void removeFromCart(IBook book);

    boolean isInCart(IBook book);

    void createOrder();

    List<IOrder> getOrders();

    int getOrdersSize();
}
