package cz.tomrechtig.presentation.controllers;

import cz.tomrechtig.data.entities.BookDAO;
import cz.tomrechtig.data.entities.OrderDAO;
import cz.tomrechtig.data.entities.UserDAO;
import cz.tomrechtig.data.entities.definition.IOrderDAO;
import cz.tomrechtig.data.services.definition.IOrderService;
import cz.tomrechtig.domain.entities.definition.IBook;
import cz.tomrechtig.domain.entities.definition.IOrder;
import cz.tomrechtig.domain.mappers.definition.IBookMapper;
import cz.tomrechtig.domain.mappers.definition.IOrderMapper;
import cz.tomrechtig.domain.mappers.definition.IUserMapper;
import cz.tomrechtig.jms.definition.IJMSProducer;
import cz.tomrechtig.presentation.controllers.definition.IUserController;
import cz.tomrechtig.presentation.entities.definition.ICurrentUser;
import cz.tomrechtig.presentation.services.definition.ISessionService;
import cz.tomrechtig.presentation.utilities.definition.IRequestUtility;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.List;
import java.util.stream.Collectors;

@Named
@ApplicationScoped
public class UserController implements IUserController {

    @Inject
    ISessionService sessionService;

    @Inject
    IOrderService orderService;

    @Inject
    IUserMapper userMapper;

    @Inject
    IBookMapper bookMapper;

    @Inject
    IOrderMapper orderMapper;

    @Inject
    IRequestUtility requestUtility;

    @Inject
    IJMSProducer jmsProducer;

    @Override
    public ICurrentUser getCurrentUser() {
        return sessionService.getCurrentUser();
    }

    @Override
    public boolean isLoggedIn() {
        return sessionService.isUserLoggedIn();
    }

    @Override
    public void logout() {
        sessionService.logout();
    }

    @Override
    public void addToCart(IBook book) {
        jmsProducer.send("[FE] User [" + getCurrentUser().getEmail() + "] added Book [" + book.getTitle() + "] to cart.");
        getCurrentUser().addToCart(book);
    }

    @Override
    public void removeFromCart(IBook book) {
        jmsProducer.send("[FE] User [" + getCurrentUser().getEmail() + "] removed Book [" + book.getTitle() + "] from cart.");
        getCurrentUser().removeFromCart(book);
    }

    @Override
    public boolean isInCart(IBook book) {
        return getCurrentUser().getCart().stream().anyMatch(cartBook -> cartBook.getId().equals(book.getId()));
    }

    @Override
    public void createOrder() {
        List<BookDAO> bookDAOList = getCurrentUser().getCart().stream().map(book -> bookMapper.toDAOClass(book)).collect(Collectors.toList());

        UserDAO userDAO = userMapper.toDAO(getCurrentUser());

        IOrderDAO orderDAO = new OrderDAO(userDAO, bookDAOList);

        orderService.createOrder(orderDAO);

        getCurrentUser().getCart().clear();

        jmsProducer.send("[FE] User [" + getCurrentUser().getEmail() + "] has created a new order.");

        requestUtility.redirect("/pages/orders.xhtml");
    }

    @Override
    public List<IOrder> getOrders() {
        List<OrderDAO> orderDAOList = orderService.getAllOrdersByUserId(this.getCurrentUser().getId());
        return orderDAOList.stream().map(orderDAO -> orderMapper.toOrder(orderDAO)).collect(Collectors.toList());
    }

    @Override
    public int getOrdersSize() {
        return orderService.getAllOrdersByUserId(this.getCurrentUser().getId()).size();
    }
}
