package cz.tomrechtig.presentation.utilities.definition;

public interface IRequestUtility {

    void redirect(String pathname);

}
