package cz.tomrechtig.presentation.utilities;

import cz.tomrechtig.presentation.utilities.definition.IRequestUtility;

import javax.enterprise.context.ApplicationScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

@ApplicationScoped
public class RequestUtility implements IRequestUtility {

    private final FacesContext facesContext = FacesContext.getCurrentInstance();

    private final ExternalContext externalContext = facesContext.getExternalContext();

    private final HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();

    @Override
    public void redirect(String pathname) {
        try {
            if (!externalContext.isResponseCommitted()) {
                externalContext.redirect(getAppRootUrl() + pathname);
                facesContext.responseComplete();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private String getAppRootUrl() throws MalformedURLException {
        return new URL(request.getScheme(),
                request.getServerName(),
                request.getServerPort(),
                request.getContextPath()).toString();
    }
}
