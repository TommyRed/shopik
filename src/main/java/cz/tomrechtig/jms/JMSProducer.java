package cz.tomrechtig.jms;

import cz.tomrechtig.jms.definition.IJMSProducer;

import javax.annotation.Resource;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.jms.JMSContext;
import javax.jms.Queue;

@ApplicationScoped
public class JMSProducer implements IJMSProducer {
    @Resource(mappedName = "java:jboss/exported/jms/queue/shopik")
    private Queue queue;

    @Inject
    JMSContext context;

    public void send(String message) {
        try {
            context.createProducer().send(queue, message);
        } catch (Exception exc) {
            exc.printStackTrace();
        }

    }
}
