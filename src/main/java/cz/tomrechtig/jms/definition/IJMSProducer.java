package cz.tomrechtig.jms.definition;

public interface IJMSProducer {
    void send(String message);
}
