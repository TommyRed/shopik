package cz.tomrechtig.domain.services.definition;

import cz.tomrechtig.data.entities.definition.IUserDAO;
import cz.tomrechtig.domain.entities.definition.IUser;
import cz.tomrechtig.presentation.entities.definition.ICurrentUser;
import cz.tomrechtig.presentation.exceptions.InvalidPasswordException;
import cz.tomrechtig.presentation.exceptions.UserNotFoundException;

public interface IAuthService {

    ICurrentUser login(String email, String password) throws UserNotFoundException, InvalidPasswordException;

    void register(IUserDAO user);

}
