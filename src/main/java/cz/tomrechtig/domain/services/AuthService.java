package cz.tomrechtig.domain.services;

import cz.tomrechtig.data.entities.definition.IUserDAO;
import cz.tomrechtig.data.services.definition.IUserService;
import cz.tomrechtig.data.utility.definition.IHibernateUtility;
import cz.tomrechtig.domain.mappers.definition.IUserMapper;
import cz.tomrechtig.domain.services.definition.IAuthService;
import cz.tomrechtig.presentation.entities.definition.ICurrentUser;
import cz.tomrechtig.presentation.exceptions.InvalidPasswordException;
import cz.tomrechtig.presentation.exceptions.UserNotFoundException;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

@ApplicationScoped
public class AuthService implements IAuthService {

    @Inject
    IHibernateUtility hibernateUtility;

    @Inject
    IUserMapper userMapper;

    @Inject
    IUserService userService;

//    private static final PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();

    @Override
    public ICurrentUser login(String email, String password) throws UserNotFoundException, InvalidPasswordException {

//        String hashedPassword = passwordEncoder.encode(password);

        IUserDAO userDAO;

        try {
            userDAO = userService.getUserByEmail(email);
        } catch (Exception exception) {
            throw new UserNotFoundException("User not found");
        }

//        if (passwordEncoder.matches(userDAO.getPassword(), hashedPassword)) {
        if (userDAO.getPassword().equals(password)) {
            return userMapper.toCurrentUser(userDAO);
        } else {
            throw new InvalidPasswordException("Invalid password");
        }
    }

    @Override
    public void register(IUserDAO user) {

//        String hashedPassword = passwordEncoder.encode(user.getPassword());
//        user.setPassword(hashedPassword);

        hibernateUtility.createEntity(user);
    }

}
