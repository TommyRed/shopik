package cz.tomrechtig.domain.exceptions;

public class InUseException extends Exception {
    public InUseException(String message) {
        super(message);
    }
}
