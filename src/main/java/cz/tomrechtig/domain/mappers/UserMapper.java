package cz.tomrechtig.domain.mappers;

import cz.tomrechtig.api.entities.definition.IRequestUser;
import cz.tomrechtig.data.entities.UserDAO;
import cz.tomrechtig.data.entities.definition.IUserDAO;
import cz.tomrechtig.domain.entities.User;
import cz.tomrechtig.domain.entities.definition.IUser;
import cz.tomrechtig.domain.mappers.definition.IUserMapper;
import cz.tomrechtig.presentation.entities.CurrentUser;
import cz.tomrechtig.presentation.entities.definition.ICurrentUser;

import javax.enterprise.context.ApplicationScoped;
import java.util.ArrayList;

@ApplicationScoped
public class UserMapper implements IUserMapper {

    @Override
    public UserDAO toDAO(ICurrentUser currentUser) {
        return new UserDAO(currentUser.getId(), currentUser.getUsername(), currentUser.getEmail(), currentUser.getPassword());
    }

    @Override
    public IUserDAO toDAO(IRequestUser requestUser) {
        return new UserDAO(requestUser.getUsername(), requestUser.getEmail(), requestUser.getPassword());
    }

    @Override
    public ICurrentUser toCurrentUser(IUserDAO user) {
        return new CurrentUser(user.getId(), user.getUsername(), user.getEmail(), user.getPassword(), new ArrayList<>());
    }

    @Override
    public IUser toUser(IUserDAO userDAO) {
        return new User(userDAO.getId(), userDAO.getUsername(), userDAO.getEmail(), userDAO.getPassword());
    }

}
