package cz.tomrechtig.domain.mappers;

import cz.tomrechtig.api.entities.definition.IRequestBook;
import cz.tomrechtig.data.entities.BookDAO;
import cz.tomrechtig.data.entities.definition.IBookDAO;
import cz.tomrechtig.domain.entities.Book;
import cz.tomrechtig.domain.entities.definition.IBook;
import cz.tomrechtig.domain.mappers.definition.IBookMapper;

import javax.enterprise.context.ApplicationScoped;
import java.util.List;
import java.util.stream.Collectors;

@ApplicationScoped
public class BookMapper implements IBookMapper {

    @Override
    public BookDAO toDAOClass(IBook book) {
        return new BookDAO(book.getId(), book.getTitle(), book.getAuthor());
    }

    @Override
    public IBookDAO toDAO(IRequestBook book) {
        return new BookDAO(book.getTitle(), book.getAuthor());
    }

    @Override
    public IBook toBook(IBookDAO bookDAO) {
        return new Book(bookDAO.getId(), bookDAO.getTitle(), bookDAO.getAuthor());
    }

    @Override
    public List<IBook> toBook(List<BookDAO> bookDAOs) {
        return bookDAOs.stream().map(this::toBook).collect(Collectors.toList());
    }
}
