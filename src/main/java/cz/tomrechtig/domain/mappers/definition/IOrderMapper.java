package cz.tomrechtig.domain.mappers.definition;

import cz.tomrechtig.api.entities.definition.IRequestOrder;
import cz.tomrechtig.data.entities.OrderDAO;
import cz.tomrechtig.data.entities.definition.IOrderDAO;
import cz.tomrechtig.domain.entities.definition.IOrder;

public interface IOrderMapper {
    OrderDAO toDAO(IRequestOrder requestOrder);

    IOrder toOrder(IOrderDAO orderDAO);
}
