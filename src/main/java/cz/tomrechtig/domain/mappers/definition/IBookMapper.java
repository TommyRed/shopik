package cz.tomrechtig.domain.mappers.definition;

import cz.tomrechtig.api.entities.definition.IRequestBook;
import cz.tomrechtig.data.entities.BookDAO;
import cz.tomrechtig.data.entities.definition.IBookDAO;
import cz.tomrechtig.domain.entities.definition.IBook;

import java.util.List;

public interface IBookMapper {

    IBookDAO toDAO(IRequestBook book);

    BookDAO toDAOClass(IBook book);

    IBook toBook(IBookDAO bookDAO);

    List<IBook> toBook(List<BookDAO> bookDAOs);
}
