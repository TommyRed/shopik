package cz.tomrechtig.domain.mappers.definition;

import cz.tomrechtig.api.entities.definition.IRequestUser;
import cz.tomrechtig.data.entities.UserDAO;
import cz.tomrechtig.data.entities.definition.IUserDAO;
import cz.tomrechtig.domain.entities.definition.IUser;
import cz.tomrechtig.presentation.entities.definition.ICurrentUser;

public interface IUserMapper {

    UserDAO toDAO(ICurrentUser currentUser);

    IUserDAO toDAO(IRequestUser requestUser);

    ICurrentUser toCurrentUser(IUserDAO user);

    IUser toUser(IUserDAO userDAO);

}
