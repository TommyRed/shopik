package cz.tomrechtig.domain.mappers;

import cz.tomrechtig.api.entities.definition.IRequestOrder;
import cz.tomrechtig.data.entities.BookDAO;
import cz.tomrechtig.data.entities.OrderDAO;
import cz.tomrechtig.data.entities.UserDAO;
import cz.tomrechtig.data.entities.definition.IBookDAO;
import cz.tomrechtig.data.entities.definition.IOrderDAO;
import cz.tomrechtig.data.entities.definition.IUserDAO;
import cz.tomrechtig.data.services.definition.IBookService;
import cz.tomrechtig.data.services.definition.IUserService;
import cz.tomrechtig.domain.entities.Order;
import cz.tomrechtig.domain.entities.definition.*;
import cz.tomrechtig.domain.mappers.definition.IBookMapper;
import cz.tomrechtig.domain.mappers.definition.IOrderMapper;
import cz.tomrechtig.domain.mappers.definition.IUserMapper;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@ApplicationScoped
public class OrderMapper implements IOrderMapper {

    @Inject
    IBookService bookService;

    @Inject
    IUserService userService;

    @Inject
    IUserMapper userMapper;

    @Inject
    IBookMapper bookMapper;

    @Override
    public OrderDAO toDAO(IRequestOrder requestOrder) {
        List<BookDAO> books = requestOrder.getBookIds().stream().map(bookId -> bookService.getBookByIdClass(bookId)).collect(Collectors.toList());

        UserDAO user = null;

        if (requestOrder.getUserId() != null) {
            user = userService.getUserById(requestOrder.getUserId());
        }

        return new OrderDAO(user, books);
    }

    @Override
    public IOrder toOrder(IOrderDAO orderDAO) {
        IUser user = userMapper.toUser(orderDAO.getUser());
        List<IBook> books = bookMapper.toBook(orderDAO.getBooks());
        return new Order(orderDAO.getId(), user, books, orderDAO.getCreatedDate(), orderDAO.getUpdatedDate());
    }
}
