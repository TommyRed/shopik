package cz.tomrechtig.domain.entities.definition;

public interface ILoginUser {

    String getUsername();
    void setUsername(String username);

    String getEmail();
    void setEmail(String email);

    String getPassword();
    void setPassword();

}
