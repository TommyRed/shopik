package cz.tomrechtig.domain.entities.definition;

public interface IBook {

    Long getId();
    void setId(Long id);

    String getTitle();
    void setTitle(String title);

    String getAuthor();
    void setAuthor(String author);

}
