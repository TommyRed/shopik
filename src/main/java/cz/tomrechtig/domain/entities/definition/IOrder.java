package cz.tomrechtig.domain.entities.definition;

import java.util.Date;
import java.util.List;

public interface IOrder {

    Long getId();

    IUser getUser();
    void setUser(IUser user);

    Long getUserId();

    List<IBook> getBooks();
    void setBooks(List<IBook> books);

    Date getCreatedDate();
    void setCreatedDate(Date createdDate);

    Date getUpdatedDate();
    void setUpdatedDate(Date updatedDate);

}
