package cz.tomrechtig.domain.entities;

import cz.tomrechtig.domain.entities.definition.IBook;
import cz.tomrechtig.domain.entities.definition.IOrder;
import cz.tomrechtig.domain.entities.definition.IUser;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class Order implements IOrder, Serializable {

    private Long id;

    private IUser user;

    private List<IBook> books;

    private Date createdDate;

    private Date updatedDate;

    public Order(Long id, IUser user, List<IBook> books, Date createdDate, Date updatedDate) {
        this.id = id;
        this.user = user;
        this.books = books;
        this.createdDate = createdDate;
        this.updatedDate = updatedDate;
    }

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public IUser getUser() {
        return user;
    }

    @Override
    public void setUser(IUser user) {
        this.user = user;
    }

    @Override
    public Long getUserId() {
        return id;
    }

    @Override
    public List<IBook> getBooks() {
        return books;
    }

    @Override
    public void setBooks(List<IBook> books) {
        this.books = books;
    }

    @Override
    public Date getCreatedDate() {
        return createdDate;
    }

    @Override
    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    @Override
    public Date getUpdatedDate() {
        return updatedDate;
    }

    @Override
    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }
}
