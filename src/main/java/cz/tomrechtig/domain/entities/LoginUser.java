package cz.tomrechtig.domain.entities;

import cz.tomrechtig.domain.entities.definition.ILoginUser;

public class LoginUser implements ILoginUser {

    private String username;

    private String email;

    private String password;

    public LoginUser(String username, String email, String password) {
        this.username = username;
        this.email = email;
        this.password = password;
    }

    @Override
    public String getUsername() {
        return username;
    }

    @Override
    public void setUsername(String username) {
        this.username = username;
    }

    @Override
    public String getEmail() {
        return this.email;
    }

    @Override
    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String getPassword() {
        return this.password;
    }

    @Override
    public void setPassword() {
        this.password = password;
    }
}
