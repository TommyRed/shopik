package cz.tomrechtig.api.resources;

import cz.tomrechtig.api.entities.RequestUser;
import cz.tomrechtig.api.resources.definition.IUserResource;
import cz.tomrechtig.data.entities.UserDAO;
import cz.tomrechtig.data.entities.definition.IUserDAO;
import cz.tomrechtig.data.services.definition.IUserService;
import cz.tomrechtig.domain.mappers.definition.IUserMapper;
import cz.tomrechtig.domain.services.definition.IAuthService;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@RequestScoped
@Path("user")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class UserResource implements IUserResource {

    @Inject
    IAuthService authService;

    @Inject
    IUserService userService;

    @Inject
    IUserMapper userMapper;

    @GET
    @Override
    public Response getAll() {
        return Response.ok(userService.getAllUsers()).build();
    }

    @GET
    @Path("{id}")
    @Override
    public Response getUser(@PathParam("id") Long id) {
        return Response.ok(userService.getUserById(id)).build();
    }

    @POST
    @Override
    public Response create(RequestUser requestUser) {
        IUserDAO userDAO = userMapper.toDAO(requestUser);
        try {
            authService.register(userDAO);
            return Response.ok(Response.Status.CREATED).build();
        } catch (Exception e) {
            e.printStackTrace();
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
    }

    @PUT
    @Path("{id}")
    @Override
    public Response update(@PathParam("id") Long id, RequestUser requestUser) {
        IUserDAO userDAO = userMapper.toDAO(requestUser);
        if (userService.updateUser(id, (UserDAO) userDAO)){
            return Response.ok(Response.Status.OK).build();
        } else {
            return Response.status(Response.Status.NOT_MODIFIED).build();
        }
    }

    @DELETE
    @Path("{id}")
    @Override
    public Response delete(@PathParam("id") Long id) {
        try {
            userService.deleteUser(id);
            return Response.ok(Response.Status.OK).build();
        } catch (Exception error) {
            error.printStackTrace();
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
    }
}
