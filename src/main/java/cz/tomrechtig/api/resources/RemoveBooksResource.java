package cz.tomrechtig.api.resources;

import cz.tomrechtig.api.resources.definition.IRemoveBooksResource;
import cz.tomrechtig.data.services.definition.IOrderService;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@RequestScoped
@Path("remove-books")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class RemoveBooksResource implements IRemoveBooksResource {

    @Inject
    IOrderService orderService;

    @PUT
    @Path("{orderId}")
    @Override
    public Response update(@PathParam("orderId") Long orderId, List<Long> bookIds) {
        try {
            orderService.removeBooks(orderId, bookIds);
            return Response.ok().build();
        } catch (Exception exception) {
            exception.printStackTrace();
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
    }
}
