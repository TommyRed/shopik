package cz.tomrechtig.api.resources.definition;

import javax.ws.rs.core.Response;
import java.util.List;

public interface IAddBooksResource {
    Response update(Long orderId, List<Long> bookIds);
}
