package cz.tomrechtig.api.resources.definition;

import javax.ws.rs.core.Response;

public interface IOrderByUserResource {

    Response get(Long userId);

}
