package cz.tomrechtig.api.resources.definition;

import cz.tomrechtig.api.entities.RequestUser;

import javax.ws.rs.core.Response;

public interface IUserResource {

    Response getAll();

    Response getUser(Long id);

    Response create(RequestUser requestUser);

    Response update(Long id, RequestUser requestUser);

    Response delete(Long id);
}
