package cz.tomrechtig.api.resources.definition;

import cz.tomrechtig.api.entities.RequestBook;

import javax.ws.rs.core.Response;

public interface IBookResource {

    Response getAll();

    Response getBook(Long id);

    Response create(RequestBook requestBook);

    Response update(Long id, RequestBook requestBook);

    Response delete(Long id);

}
