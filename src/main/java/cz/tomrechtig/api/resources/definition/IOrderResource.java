package cz.tomrechtig.api.resources.definition;

import cz.tomrechtig.api.entities.RequestOrder;

import javax.ws.rs.core.Response;

public interface IOrderResource {

    Response getAll();

    Response getOrder(Long id);

    Response create(RequestOrder requestBook);

    Response update(Long orderId, RequestOrder requestOrder);

    Response delete(Long orderId);

}
