package cz.tomrechtig.api.resources.definition;

import cz.tomrechtig.api.entities.LoginUser;

import javax.ws.rs.core.Response;

public interface ILoginResource {
    Response login(LoginUser loginUser);
}
