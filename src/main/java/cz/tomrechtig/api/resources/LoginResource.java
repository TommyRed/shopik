package cz.tomrechtig.api.resources;

import cz.tomrechtig.api.entities.LoginUser;
import cz.tomrechtig.api.resources.definition.ILoginResource;
import cz.tomrechtig.domain.services.definition.IAuthService;
import cz.tomrechtig.presentation.exceptions.InvalidPasswordException;
import cz.tomrechtig.presentation.exceptions.UserNotFoundException;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@RequestScoped
@Path("login")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class LoginResource implements ILoginResource {

    @Inject
    IAuthService authService;

    @POST
    @Override
    public Response login(LoginUser loginUser) {
        try {
            authService.login(loginUser.getEmail(), loginUser.getPassword());
            return Response.ok().build();
        } catch (UserNotFoundException userNotFoundException) {
            return Response.status(Response.Status.NOT_FOUND).build();
        } catch (InvalidPasswordException invalidPasswordException) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
    }
}
