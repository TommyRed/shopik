package cz.tomrechtig.api.resources;

import cz.tomrechtig.api.entities.RequestOrder;
import cz.tomrechtig.api.resources.definition.IOrderResource;
import cz.tomrechtig.data.entities.OrderDAO;
import cz.tomrechtig.data.services.definition.IOrderService;
import cz.tomrechtig.domain.mappers.definition.IOrderMapper;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@RequestScoped
@Path("order")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class OrderResource implements IOrderResource {

    @Inject
    IOrderService orderService;

    @Inject
    IOrderMapper orderMapper;

    @GET
    @Override
    public Response getAll() {
        return Response.ok(orderService.getAllOrders()).build();
    }

    @GET
    @Path("{id}")
    @Override
    public Response getOrder(@PathParam("id") Long id) {
        return Response.ok(orderService.getAllOrdersByUserId(id)).build();
    }

    @POST
    @Override
    public Response create(RequestOrder requestBook) {
        OrderDAO orderDAO = orderMapper.toDAO(requestBook);

        try {
            orderService.createOrder(orderDAO);
            return Response.ok(Response.Status.CREATED).build();
        }  catch(Exception e) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
    }

    @PUT
    @Path("{id}")
    @Override
    public Response update(@PathParam("id") Long orderId, RequestOrder requestOrder) {
        OrderDAO orderDAO = orderMapper.toDAO(requestOrder);
        try {
            orderService.updateOrder(orderDAO, orderId);
            return Response.ok().build();
        } catch (Exception exception) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
    }

    @DELETE
    @Path("{id}")
    @Override
    public Response delete(@PathParam("id") Long orderId) {
        try {
            orderService.deleteOrder(orderId);
            return Response.ok().build();
        } catch (Exception exception) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
    }

}
