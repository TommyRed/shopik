package cz.tomrechtig.api.resources;

import cz.tomrechtig.api.resources.definition.IAddBooksResource;
import cz.tomrechtig.data.entities.BookDAO;
import cz.tomrechtig.data.services.definition.IBookService;
import cz.tomrechtig.data.services.definition.IOrderService;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.stream.Collectors;

@RequestScoped
@Path("add-books")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class AddBooksResource implements IAddBooksResource {

    @Inject
    IBookService bookService;

    @Inject
    IOrderService orderService;

    @PUT
    @Path("{orderId}")
    @Override
    public Response update(@PathParam("orderId") Long orderId, List<Long> bookIds) {

        List<BookDAO> bookDAOs = bookIds.stream().map(bookId -> bookService.getBookById(bookId)).collect(Collectors.toList());

        try {
            orderService.addBooks(orderId, bookDAOs);
            return Response.ok().build();
        } catch (Exception exception) {
            exception.printStackTrace();
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
    }
}
