package cz.tomrechtig.api.resources;

import cz.tomrechtig.api.resources.definition.IOrderByUserResource;
import cz.tomrechtig.data.services.definition.IOrderService;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@RequestScoped
@Path("orders")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class OrderByUserResource implements IOrderByUserResource {

    @Inject
    IOrderService orderService;

    @GET
    @Path("{id}")
    @Override
    public Response get(@PathParam("id") Long userId) {
        return Response.ok(orderService.getAllOrdersByUserId(userId)).build();
    }
}
