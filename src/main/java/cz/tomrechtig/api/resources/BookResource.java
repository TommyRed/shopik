package cz.tomrechtig.api.resources;

import cz.tomrechtig.api.entities.RequestBook;
import cz.tomrechtig.api.resources.definition.IBookResource;
import cz.tomrechtig.data.entities.BookDAO;
import cz.tomrechtig.data.entities.definition.IBookDAO;
import cz.tomrechtig.data.services.definition.IBookService;
import cz.tomrechtig.domain.mappers.definition.IBookMapper;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@RequestScoped
@Path("book")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class BookResource implements IBookResource {

    @Inject
    IBookService bookService;

    @Inject
    IBookMapper bookMapper;

    @GET
    @Override
    public Response getAll() {
        return Response.ok(bookService.getAllBooks()).build();
    }

    @GET
    @Path("{id}")
    @Override
    public Response getBook(@PathParam("id") Long id) {
        return Response.ok(bookService.getBookById(id)).build();
    }

    @POST
    @Override
    public Response create(RequestBook requestBook) {
        IBookDAO book = bookMapper.toDAO(requestBook);
        try {
            bookService.createBook(book);
            return Response.ok(Response.Status.CREATED).build();
        } catch (Exception e) {
            e.printStackTrace();
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
    }

    @PUT
    @Path("{id}")
    @Override
    public Response update(@PathParam("id") Long id, RequestBook requestBook) {
        IBookDAO bookDAO = bookMapper.toDAO(requestBook);
        if (bookService.updateBook(id, (BookDAO) bookDAO)) {
            return Response.ok(Response.Status.OK).build();
        } else {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
    }

    @DELETE
    @Path("{id}")
    @Override
    public Response delete(@PathParam("id") Long id) {
        try {
            bookService.deleteBook(id);
            return Response.ok(Response.Status.OK).build();
        } catch (Exception error) {
            error.printStackTrace();
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
    }
}
