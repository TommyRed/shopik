package cz.tomrechtig.api.entities.definition;

public interface ILoginUser {

    String getPassword();
    void setPassword(String password);

    String getEmail();
    void setEmail(String email);

}
