package cz.tomrechtig.api.entities.definition;

public interface IRequestBook {

    String getTitle();
    void setTitle(String title);

    String getAuthor();
    void setAuthor(String author);

}
