package cz.tomrechtig.api.entities.definition;

import java.io.Serializable;
import java.util.List;

public interface IRequestOrder extends Serializable {

    Long getUserId();
    void setUserId(Long userId);

    List<Long> getBookIds();
    void setBookIds(List<Long> bookIds);

}
