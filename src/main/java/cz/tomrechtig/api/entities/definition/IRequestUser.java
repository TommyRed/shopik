package cz.tomrechtig.api.entities.definition;

public interface IRequestUser {

    String getUsername();
    void setUsername(String username);

    String getEmail();
    void setEmail(String email);

    String getPassword();
    void setPassword(String password);

}
