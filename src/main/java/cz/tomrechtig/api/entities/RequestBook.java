package cz.tomrechtig.api.entities;

import cz.tomrechtig.api.entities.definition.IRequestBook;

public class RequestBook implements IRequestBook {

    private String title;

    private String author;

    public RequestBook() {
    }

    public RequestBook(String title, String author) {
        this.title = title;
        this.author = author;
    }

    @Override
    public String getTitle() {
        return title;
    }

    @Override
    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public String getAuthor() {
        return author;
    }

    @Override
    public void setAuthor(String author) {
        this.author = author;
    }
}
