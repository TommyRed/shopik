package cz.tomrechtig.api.entities;

import cz.tomrechtig.api.entities.definition.IRequestOrder;

import java.util.List;

public class RequestOrder implements IRequestOrder {

    private Long userId;

    private List<Long> bookIds;

    public RequestOrder() {
    }

    public RequestOrder(Long userId, List<Long> bookIds) {
        this.userId = userId;
        this.bookIds = bookIds;
    }

    @Override
    public Long getUserId() {
        return userId;
    }

    @Override
    public void setUserId(Long userId) {
        this.userId = userId;
    }

    @Override
    public List<Long> getBookIds() {
        return bookIds;
    }

    @Override
    public void setBookIds(List<Long> bookIds) {
        this.bookIds = bookIds;
    }
}
