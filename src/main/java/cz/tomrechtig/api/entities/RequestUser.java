package cz.tomrechtig.api.entities;

import cz.tomrechtig.api.entities.definition.IRequestUser;

public class RequestUser implements IRequestUser {

    private String username;

    private String email;

    private String password;

    public RequestUser() {
    }

    public RequestUser(String username, String email, String password) {
        this.username = username;
        this.email = email;
        this.password = password;
    }

    @Override
    public String getUsername() {
        return username;
    }

    @Override
    public void setUsername(String username) {
        this.username = username;
    }

    @Override
    public String getEmail() {
        return email;
    }

    @Override
    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public void setPassword(String password) {
        this.password = password;
    }
}
