package cz.tomrechtig.data.entities.definition;

public interface IUserDAO {

    Long getId();

    String getUsername();
    void setUsername(String username);

    String getEmail();
    void setEmail(String email);

    String getPassword();
    void setPassword(String password);

}
