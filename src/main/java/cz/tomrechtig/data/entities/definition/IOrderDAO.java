package cz.tomrechtig.data.entities.definition;

import cz.tomrechtig.data.entities.BookDAO;
import cz.tomrechtig.data.entities.UserDAO;

import java.util.Date;
import java.util.List;

public interface IOrderDAO {

    Long getId();

    UserDAO getUser();
    void setUser(UserDAO user);

    List<BookDAO> getBooks();
    void setBooks(List<BookDAO> books);

    Date getCreatedDate();
    void setCreatedDate(Date createdDate);

    Date getUpdatedDate();
    void setUpdatedDate(Date updatedDate);

}
