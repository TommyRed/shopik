package cz.tomrechtig.data.entities.definition;

public interface IBookDAO {

    Long getId();
    void setId(Long id);

    String getTitle();
    void setTitle(String title);

    String getAuthor();
    void setAuthor(String author);

}
