package cz.tomrechtig.data.entities;

import cz.tomrechtig.data.entities.definition.IOrderDAO;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "orders")
public class OrderDAO implements IOrderDAO {

    @Id
    @GeneratedValue(
            strategy = GenerationType.AUTO,
            generator = "native"
    )
    @GenericGenerator(
            name = "native",
            strategy = "native"
    )
    @Column(unique = true, nullable = false, updatable = false)
    private Long id;

    @ManyToOne
    private UserDAO user;

    @ManyToMany
    @JoinTable(name = "book_order",
            joinColumns = { @JoinColumn(name = "order_id") },
            inverseJoinColumns = { @JoinColumn(name = "book_id") })
    private List<BookDAO> books;

    @Column(nullable = false)
    private Date createdDate;

    @Column(nullable = false)
    private Date updatedDate;

    public OrderDAO() {}
    
    public OrderDAO(Long id, UserDAO user, List<BookDAO> books, Date createdDate, Date updatedDate) {
        this.id = id;
        this.user = user;
        this.books = books;
        this.createdDate = createdDate;
        this.updatedDate = updatedDate;
    }

    public OrderDAO(UserDAO user, List<BookDAO> books, Date createdDate, Date updatedDate) {
        this.user = user;
        this.books = books;
        this.createdDate = createdDate;
        this.updatedDate = updatedDate;
    }

    public OrderDAO(UserDAO user, List<BookDAO> books) {
        this(user, books, java.sql.Date.valueOf(LocalDate.now()), java.sql.Date.valueOf(LocalDate.now()));
    }

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public UserDAO getUser() {
        return user;
    }

    @Override
    public void setUser(UserDAO user) {
        this.user = user;
    }

    @Override
    public List<BookDAO> getBooks() {
        return books;
    }

    @Override
    public void setBooks(List<BookDAO> books) {
        this.books = books;
    }

    @Override
    public Date getCreatedDate() {
        return createdDate;
    }

    @Override
    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    @Override
    public Date getUpdatedDate() {
        return updatedDate;
    }

    @Override
    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }
}
