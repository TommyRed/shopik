package cz.tomrechtig.data.utility;

import cz.tomrechtig.data.utility.definition.CriteriaBuilderFunction;
import cz.tomrechtig.data.utility.definition.IHibernateUtility;
import cz.tomrechtig.data.utility.definition.SessionFunction;
import cz.tomrechtig.domain.exceptions.InUseException;
import org.hibernate.exception.ConstraintViolationException;

import javax.enterprise.context.ApplicationScoped;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

@ApplicationScoped
public class HibernateUtility implements IHibernateUtility {

    private static EntityManager entityManager;

    private static void openSession() {
        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("shopik");
        entityManager = entityManagerFactory.createEntityManager();
    }

    @Override
    public <T> T getOne(Class<T> type, CriteriaBuilderFunction<T> function) {
        AtomicReference<T> reference = new AtomicReference<>();

        this.getCurrentSession(entityManager -> {
            CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();

            CriteriaQuery<T> criteriaQuery = criteriaBuilder.createQuery(type);

            Root<T> root = criteriaQuery.from(type);

            CriteriaQuery<T> query = function.buildCriteria(criteriaQuery, criteriaBuilder, root);

            T resultList = entityManager.createQuery(query).getSingleResult();

            reference.set(resultList);
        });

        return reference.get();
    }

    @Override
    public <T> List<T> getList(Class<T> type, CriteriaBuilderFunction<T> function) {
        AtomicReference<List<T>> reference = new AtomicReference<>();

        this.getCurrentSession(entityManager -> {
            CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();

            CriteriaQuery<T> criteriaQuery = criteriaBuilder.createQuery(type);

            Root<T> root = criteriaQuery.from(type);

            CriteriaQuery<T> query = function.buildCriteria(criteriaQuery, criteriaBuilder, root);

            List<T> resultList = entityManager.createQuery(query).getResultList();

            reference.set(resultList);
        });

        return reference.get();
    }

    @Override
    public void getCurrentSession(SessionFunction function) {
        if (HibernateUtility.entityManager == null) {
            HibernateUtility.openSession();
        }

        try {
            HibernateUtility.entityManager.getTransaction().begin();
            function.invoke(HibernateUtility.entityManager);
            HibernateUtility.entityManager.getTransaction().commit();
        } catch (Exception exception) {
            HibernateUtility.entityManager.getTransaction().rollback();
            exception.printStackTrace();
            throw exception;
        }
    }

    @Override
    public <T> List<T> getAll(Class<T> type) {
        return this.getList(type, ((query, builder, root) -> query.select(root)));
    }

    @Override
    public <T> T getById(Class<T> type, Long id) {
        return this.getOne(type, ((query, builder, root) -> query.where(builder.equal(root.get("id"), id))));
    }

    @Override
    public <T> void createEntity(T entityDAO) {
        try {
            this.getCurrentSession(entityManager -> {
                entityManager.persist(entityDAO);
            });
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }

    @Override
    public <T> void deleteEntity(Class<T> type, Long entityId) {
        this.getCurrentSession(entityManager1 -> {
            T entity = entityManager.find(type, entityId);
            entityManager.remove(entity);
        });
    }
}
