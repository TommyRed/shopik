package cz.tomrechtig.data.utility.definition;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

public interface CriteriaBuilderFunction<T> {
    CriteriaQuery<T> buildCriteria(CriteriaQuery<T> query, CriteriaBuilder builder, Root<T> root);
}
