package cz.tomrechtig.data.utility.definition;

import javax.persistence.EntityManager;

public interface SessionFunction {
    void invoke(EntityManager entityManager);
}
