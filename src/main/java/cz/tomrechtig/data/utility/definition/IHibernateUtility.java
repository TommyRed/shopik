package cz.tomrechtig.data.utility.definition;

import java.util.List;

public interface IHibernateUtility {

    void getCurrentSession(SessionFunction function);

    <T> T getOne(Class<T> type, CriteriaBuilderFunction<T> function);

    <T> List<T> getList(Class<T> type, CriteriaBuilderFunction<T> function);

    <T> List<T> getAll(Class<T> type);

    <T> T getById(Class<T> type, Long id);

    <T> void createEntity(T entityDAO);

    <T> void deleteEntity(Class<T> type, Long entityId);

}
