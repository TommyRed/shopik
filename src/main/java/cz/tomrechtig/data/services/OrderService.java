package cz.tomrechtig.data.services;

import cz.tomrechtig.data.entities.BookDAO;
import cz.tomrechtig.data.entities.OrderDAO;
import cz.tomrechtig.data.entities.definition.IBookDAO;
import cz.tomrechtig.data.entities.definition.IOrderDAO;
import cz.tomrechtig.data.services.definition.IOrderService;
import cz.tomrechtig.data.utility.definition.IHibernateUtility;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

@ApplicationScoped
public class OrderService implements IOrderService {

    @Inject
    IHibernateUtility hibernateUtility;

    @Override
    public List<OrderDAO> getAllOrders() {
        return hibernateUtility.getAll(OrderDAO.class);
    }

    @Override
    public List<OrderDAO> getAllOrdersByUserId(Long userId) {
        return hibernateUtility.getList(OrderDAO.class, (query, builder, root) -> query.where(builder.equal(root.get("user"), userId)));
    }

    @Override
    public void createOrder(IOrderDAO orderDAO) {
        hibernateUtility.createEntity(orderDAO);
    }

    @Override
    public void updateOrder(OrderDAO orderDAO, Long orderId) {
        hibernateUtility.getCurrentSession(entityManager -> {
            OrderDAO order = entityManager.getReference(OrderDAO.class, orderId);

            if (orderDAO.getBooks() != null) {
                order.setBooks(orderDAO.getBooks());
            }

            order.setUpdatedDate(java.sql.Date.valueOf(LocalDate.now()));
        });
    }

    @Override
    public void addBooks(Long orderId, List<BookDAO> bookDAOs) {
        hibernateUtility.getCurrentSession(entityManager -> {
            OrderDAO orderDAO = entityManager.find(OrderDAO.class, orderId);
            bookDAOs.forEach(bookDAO -> orderDAO.getBooks().add(bookDAO));
        });
    }

    @Override
    public void removeBooks(Long orderId, List<Long> bookDAOs) {
        hibernateUtility.getCurrentSession(entityManager -> {
            OrderDAO orderDAO = entityManager.find(OrderDAO.class, orderId);
            List<BookDAO> redactedBookList = orderDAO.getBooks().stream().filter(bookDAO -> !bookDAOs.contains(bookDAO.getId())).collect(Collectors.toList());
            orderDAO.setBooks(redactedBookList);
        });
    }

    @Override
    public void deleteOrder(Long orderId) {
        hibernateUtility.deleteEntity(OrderDAO.class, orderId);
    }
}
