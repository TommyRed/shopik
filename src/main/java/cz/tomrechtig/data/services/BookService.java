package cz.tomrechtig.data.services;

import cz.tomrechtig.data.entities.BookDAO;
import cz.tomrechtig.data.entities.definition.IBookDAO;
import cz.tomrechtig.data.utility.definition.IHibernateUtility;
import cz.tomrechtig.data.services.definition.IBookService;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

@ApplicationScoped
public class BookService implements IBookService {

    @Inject
    IHibernateUtility hibernateUtility;

    @Override
    public List<BookDAO> getAllBooks() {
        return hibernateUtility.getAll(BookDAO.class);
    }

    @Override
    public BookDAO getBookById(Long id) {
        return hibernateUtility.getById(BookDAO.class, id);
    }

    @Override
    public BookDAO getBookByIdClass(Long id) {
        return hibernateUtility.getById(BookDAO.class, id);
    }

    @Override
    public void createBook(IBookDAO book) {
        hibernateUtility.createEntity(book);
    }

    @Override
    public boolean updateBook(Long bookId, BookDAO bookDAO) {
        AtomicBoolean result = new AtomicBoolean(false);

        hibernateUtility.getCurrentSession(entityManager -> {
            BookDAO book = entityManager.getReference(BookDAO.class, bookId);

            if (bookDAO.getAuthor() != null) {
                book.setAuthor(bookDAO.getAuthor());
            }

            if (bookDAO.getTitle() != null) {
                book.setTitle(bookDAO.getTitle());
            }

            result.set(true);
        });

        return result.get();
    }

    @Override
    public void deleteBook(Long bookId) {
        hibernateUtility.deleteEntity(BookDAO.class, bookId);
    }
}
