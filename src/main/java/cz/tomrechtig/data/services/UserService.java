package cz.tomrechtig.data.services;

import cz.tomrechtig.data.entities.UserDAO;
import cz.tomrechtig.data.entities.definition.IUserDAO;
import cz.tomrechtig.data.utility.definition.IHibernateUtility;
import cz.tomrechtig.data.services.definition.IUserService;
import cz.tomrechtig.domain.exceptions.InUseException;
import org.hibernate.exception.ConstraintViolationException;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;

@ApplicationScoped
public class UserService implements IUserService {

    @Inject
    IHibernateUtility hibernateUtility;

    @Override
    public List<UserDAO> getAllUsers() {
        return hibernateUtility.getAll(UserDAO.class);
    }

    @Override
    public UserDAO getUserById(Long id) {
        return hibernateUtility.getById(UserDAO.class, id);
    }

    @Override
    public UserDAO getUserByEmail(String email) {
        return hibernateUtility.getOne(UserDAO.class, ((query, builder, root) -> query.where(builder.equal(root.get("email"), email))));
    }

    @Override
    public void createUser(IUserDAO userDAO) {
        hibernateUtility.createEntity(userDAO);
    }

    @Override
    public boolean updateUser(Long userId, UserDAO userDAO) {
        AtomicBoolean result = new AtomicBoolean(false);

        hibernateUtility.getCurrentSession(entityManager -> {
            UserDAO user = entityManager.getReference(UserDAO.class, userId);

            if (userDAO.getEmail() != null) {
                user.setEmail(userDAO.getEmail());
            }

            if (userDAO.getUsername() != null) {
                user.setUsername(userDAO.getUsername());
            }

            if (userDAO.getPassword() != null) {
                user.setPassword(userDAO.getPassword());
            }

            result.set(true);
        });

        return result.get();
    }

    @Override
    public void deleteUser(Long userId) {
        hibernateUtility.deleteEntity(UserDAO.class, userId);
    }
}
