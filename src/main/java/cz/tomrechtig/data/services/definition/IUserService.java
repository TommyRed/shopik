package cz.tomrechtig.data.services.definition;

import cz.tomrechtig.data.entities.UserDAO;
import cz.tomrechtig.data.entities.definition.IUserDAO;
import cz.tomrechtig.domain.exceptions.InUseException;

import java.util.List;

public interface IUserService {

    List<UserDAO> getAllUsers();

    UserDAO getUserById(Long id);

    UserDAO getUserByEmail(String email);

    void createUser(IUserDAO userDAO);

    boolean updateUser(Long userId, UserDAO userDAO);

    void deleteUser(Long userId) throws InUseException;

}
