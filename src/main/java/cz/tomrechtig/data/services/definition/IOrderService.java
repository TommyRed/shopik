package cz.tomrechtig.data.services.definition;

import cz.tomrechtig.data.entities.BookDAO;
import cz.tomrechtig.data.entities.OrderDAO;
import cz.tomrechtig.data.entities.definition.IOrderDAO;

import java.util.List;

public interface IOrderService {

    List<OrderDAO> getAllOrders();

    List<OrderDAO> getAllOrdersByUserId(Long userId);

    void createOrder(IOrderDAO orderDAO);

    void updateOrder(OrderDAO orderDAO, Long orderId);

    void addBooks(Long orderId, List<BookDAO> bookDAOs);

    void removeBooks(Long orderId, List<Long> bookIds);

    void deleteOrder(Long orderId);

}
