package cz.tomrechtig.data.services.definition;

import cz.tomrechtig.data.entities.BookDAO;
import cz.tomrechtig.data.entities.definition.IBookDAO;
import cz.tomrechtig.domain.exceptions.InUseException;

import java.util.List;

public interface IBookService {

    List<BookDAO> getAllBooks();

    BookDAO getBookById(Long id);

    BookDAO getBookByIdClass(Long id);

    void createBook(IBookDAO book);

    boolean updateBook(Long bookId, BookDAO bookDAO);

    void deleteBook(Long bookId) throws InUseException;

}
