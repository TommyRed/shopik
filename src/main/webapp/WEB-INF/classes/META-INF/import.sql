INSERT INTO users (id, username, email, password) VALUES (1, 'tomas', 'tom@tom.cz', 'tomtomtom')

INSERT INTO books (id, author, title) VALUES (1, 'J.R.R. Tolkien', 'Fellowship of the Ring');
INSERT INTO books (id, author, title) VALUES (2, 'J.R.R. Tolkien', 'Two Towers');
INSERT INTO books (id, author, title) VALUES (3, 'J.R.R. Tolkien', 'The return of the King');